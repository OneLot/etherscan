create table "holder_table"
(
    id   serial primary key ,
    token_id int REFERENCES token_table,

    address varchar(100) not null,
    quantity float not null,

    as_of_datetime TIMESTAMP not null DEFAULT NOW()

);

insert into holder_table(id, token_id, address, quantity)
values (1, 1, '43B', 11), (2, 1, 'Brevoort', 12);

CREATE INDEX "as_of_idx" ON "holder_table" ("as_of_datetime");
CREATE INDEX "address_idx" ON "holder_table" ("address");