import logging
from typing import List

from db import dbs
from models.token_holder import Token, Holder

sesh = dbs['posgresql-connected-sesh']


def get_token_by_name(name: str):
    return sesh.query(Token).where(Token.name == name).first()


def bulk_save_token_holders(holders: List[Holder]) -> bool:
    sesh.add_all(holders)
    try:
        sesh.commit()
    except Exception as exc:
        logging.error(exc, exc_info=True)
        return False
    return True
