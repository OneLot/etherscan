from sqlalchemy import create_engine, Engine
from sqlalchemy.orm import declarative_base, sessionmaker

from config import config

db = config['database']
engine: Engine = create_engine(f"postgresql+psycopg2://{db['username']}:{db['password']}@"
                               f"{db['host']}:{db['port']}/{db['db']}")
Base = declarative_base()
metadata = Base.metadata

Session = sessionmaker(bind=engine)
session = Session()

dbs = {
    'posgresql-connected-sesh': session
}

