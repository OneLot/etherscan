import logging

import yaml

config = None

with open('prod.yml') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        logging.error(exc, exc_info=True)
