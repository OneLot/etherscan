import json

from requests import Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects

from config import config

url = 'https://sandbox-api.coinmarketcap.com/v2/cryptocurrency/quotes/latest'
url = 'https://pro-api.coinmarketcap.com/v2/cryptocurrency/quotes/latest'
headers = {
    'Accepts': 'application/json',
    'X-CMC_PRO_API_KEY': f'{config["coin-market-cap"]["api-token"]}',
}

session = Session()
session.headers.update(headers)


def get_price_metrics(crypto: str = 'pepe') -> dict:
    """
    :param crypto:
    :return: {
        'price' = {float} 1.5776353855255974e-06
        'volume_24h' = {float} 193065070.1247842
        'volume_change_24h' = {float} -11.1034
        'percent_change_1h' = {float} -0.5294913
        'percent_change_24h' = {float} 0.28386787
        'percent_change_7d' = {float} -5.48813737
        'percent_change_30d' = {float} 380.59124639
        'percent_change_60d' = {float} 2675.05664245
        'percent_change_90d' = {float} 2675.05664245
        'market_cap' = {float} 618101767.6950738
        'market_cap_dominance' = {float} 0.0542
        'fully_diluted_market_cap' = {float} 663695430.34
        'tvl' = {NoneType} None
        'last_updated' = {str} '2023-05-23T10:00:00.000Z'
    }
    """
    parameters = {
        'slug': crypto
    }
    try:
        response = session.get(url, params=parameters)
        return list(response.json()['data'].values())[0]['quote']['USD']
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
