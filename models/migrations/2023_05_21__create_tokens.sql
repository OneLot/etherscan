create table "token_table"
(
    id               serial primary key,
    name             varchar(100) not null,
    contract_address varchar(100) not null,
    as_of_datetime   TIMESTAMP    not null DEFAULT NOW()
);

insert into token_table("id", "name", "contract_address")
values (1,
        'PePe',
        '0x6982508145454ce325ddbe47a25d4ec3d2311933');