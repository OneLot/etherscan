import datetime

import requests

from config import config
from models.token_holder import Holder
from repository.token_repository import bulk_save_token_holders


def build_url(offset=1):
    offset_limit = 10e3
    return f"""https://api.etherscan.io/api?module=token&action=tokenholderlist&
contractaddress={config['tokens']['pepe']}&
page={offset}&
offset={int(offset_limit)}&
apikey={config['etherscan']['api-token']}""".strip().replace('\n', '')


token_holder_list = build_url()
first_request = requests.get(token_holder_list)
token_qty_by_address = {}

for r in first_request.json()['result']:
    token_qty_by_address[r['TokenHolderAddress']] = float(r['TokenHolderQuantity']) / 1e18

# Will Work until there are 1,000,000 holders.
for i in range(2, 100):
    token_holder_list = build_url(i)
    req = requests.get(token_holder_list)
    for r in req.json()['result']:
        token_qty_by_address[r['TokenHolderAddress']] = float(r['TokenHolderQuantity']) / 1e18

to_commit = []
now = datetime.datetime.now()
for k, v in token_qty_by_address.items():
    to_commit.append(
        Holder(
            token_id=1,
            address=k,
            quantity=v,
            as_of_datetime=now
        )
    )
bulk_save_token_holders(to_commit)
