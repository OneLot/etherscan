from __future__ import annotations

import datetime
from typing import List

from sqlalchemy import Column, String, DateTime, Integer, ForeignKey, Float
from sqlalchemy.orm import relationship, Mapped, mapped_column

from db import Base


class Token(Base):
    __tablename__ = "token_table"

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    holders: Mapped[List["Holder"]] = relationship(back_populates="token")

    name: str = Column(String, nullable=False)
    contract_address: str = Column(String, nullable=False)
    as_of_datetime: datetime.datetime = Column(DateTime, nullable=False)


class Holder(Base):
    __tablename__ = "holder_table"

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    token_id: Mapped[int] = mapped_column(ForeignKey("token_table.id"))
    token: Mapped["Token"] = relationship(back_populates="holders")

    address: str = Column(String, nullable=False)
    quantity: float = Column(Float, nullable=False)

    as_of_datetime: datetime.datetime = Column(DateTime, nullable=False)

    def __repr__(self):
        return f"{self.address} | {self.quantity} | {self.as_of_datetime.date()}"
