import base64
import io
import logging

import numpy
import pandas
from matplotlib import pyplot as plt

import mailman
from coin_market_cap_api import get_price_metrics
from db import dbs
from models.token_holder import Token

sesh = dbs['posgresql-connected-sesh']
pepe: Token = sesh.query(Token).where(Token.name == 'PePe').first()

set_holders_as_of_datetime = sorted(set([holder.as_of_datetime for holder in pepe.holders]))

latest_holders_as_of = set_holders_as_of_datetime[-1]
previous_day_as_of = set_holders_as_of_datetime[-5]
one_week_as_of = set_holders_as_of_datetime[-10]  # change to -35 when data is there.

latest_holders = [holder for holder in pepe.holders if holder.as_of_datetime == latest_holders_as_of]
latest_holders_df = pandas.DataFrame.from_records([vars(holder) for holder in latest_holders])
latest_holders_df = latest_holders_df[['quantity', 'address']]
previous_day_holders = [holder for holder in pepe.holders if holder.as_of_datetime == previous_day_as_of]
previous_day_holders_df = pandas.DataFrame.from_records([vars(holder) for holder in previous_day_holders])
previous_day_holders_df = previous_day_holders_df[['quantity', 'address']]
one_week_ago_holders = [holder for holder in pepe.holders if holder.as_of_datetime == previous_day_as_of]
one_week_ago_holders_df = pandas.DataFrame.from_records([vars(holder) for holder in one_week_ago_holders])
one_week_ago_holders_df = one_week_ago_holders_df[['quantity', 'address']]

holders_df = latest_holders_df.merge(previous_day_holders_df,
                                     left_on='address',
                                     right_on='address',
                                     suffixes=('_latest', '_yesterday'))

holders_df['net_flow'] = holders_df['quantity_latest'] - holders_df['quantity_yesterday']
price_metrics = get_price_metrics(crypto='pepe')
last_px = price_metrics['price']
px_data_df = pandas.DataFrame.from_dict([price_metrics]).T

holders_df['net_flow_k_usd'] = holders_df['net_flow'] * price_metrics['price'] / 1e3

new_buyers = latest_holders_df[~latest_holders_df['address'].isin(previous_day_holders_df['address'])]
complete_sellers = previous_day_holders_df[~previous_day_holders_df['address'].isin(latest_holders_df['address'])]

new_buyers['net_flow_k_usd'] = new_buyers['quantity'] * price_metrics['price'] / 1e3
complete_sellers['net_flow_k_usd'] = -complete_sellers['quantity'] * price_metrics['price'] / 1e3
holders_df['net_flow_k_usd'] = holders_df['net_flow_k_usd'].astype(int)

new_buyers_description = new_buyers.describe()
complete_sellers_description = complete_sellers.describe()
holders_df_description = holders_df.describe()
new_buyers_description['net_flow_k_usd'] = new_buyers_description["net_flow_k_usd"].map('{:,.2f}'.format)
complete_sellers_description['net_flow_k_usd'] = complete_sellers_description["net_flow_k_usd"].map('{:,.2f}'.format)
holders_df_description['net_flow_k_usd'] = holders_df_description["net_flow_k_usd"].map('{:,.2f}'.format)

latest_holders_described = latest_holders_df.describe()
latest_holders_described = latest_holders_described.loc[['25%', '50%', '75%']]

one_week_ago_holders_described = one_week_ago_holders_df.describe()
one_week_ago_holders_described = one_week_ago_holders_described.loc[['25%', '50%', '75%']]

last_vs_today_float_distribution_df = latest_holders_described.join(one_week_ago_holders_described,
                                                                    lsuffix='_last',
                                                                    rsuffix='_one_week')

# Creating a Plot
fig = plt.figure(figsize=(15, 10))


def absolute_value(val):
    a = numpy.round(val / 100. * last_vs_today_float_distribution_df['quantity_last'].sum(), 0)
    return a


last_vs_today_float_distribution_df.plot.pie(subplots=True, autopct=absolute_value)

fig.show()

io_bytes = io.BytesIO()
plt.savefig(io_bytes, format='jpg')
io_bytes.seek(0)
my_base64_jpgData = base64.b64encode(io_bytes.read()).decode()

email_body = f"<h1>Price Metrics</h1><br>{px_data_df.to_html()}</h1><br>" \
             f"<h1>New Buyers</h1><br>{new_buyers_description.to_html()}</h1><br>" \
             f"<h1>Holders</h1><br>{holders_df_description.to_html()}</h1><br>" \
             f"<h1>Dumpers</h1><br>{complete_sellers_description.to_html()}</h1><br>" \
             f"<h1>Coin Pct Float Distribution</h1><br><img src=\"data:image/png;base64,{my_base64_jpgData}\">"

mailman.send_email(subject='PePe Daily', body=email_body, recipients=['edwardbarthelemy@protonmail.ch'])
logging.info('pepe report sent')
